package com.example.dttrealestate.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class House implements Serializable, Comparable<House> {
    private int distanceTo;
    @SerializedName("id")
    @Expose()
    private String id;
    @SerializedName("image")
    @Expose()
    private String image;
    @SerializedName("price")
    @Expose()
    private int price;
    @SerializedName("bedrooms")
    @Expose()
    private int bedrooms;
    @SerializedName("bathrooms")
    @Expose()
    private int bathrooms;
    @SerializedName("size")
    @Expose()
    private int size;
    @SerializedName("description")
    @Expose()
    private String description;
    @SerializedName("zip")
    @Expose()
    private String zip;
    @SerializedName("city")
    @Expose()
    private String city;
    @SerializedName("latitude")
    @Expose()
    private int latitude;
    @SerializedName("longitude")
    @Expose()
    private int longitude;
    @SerializedName("createdDate")
    @Expose()
    private String createdDate;

    public int getDistanceTo() {
        return distanceTo;
    }
    public void setDistanceTo(int distanceTo) {
        this.distanceTo = distanceTo;
    }
    public String getId() {
        return id;
    }
    public String getImage() {
        return image;
    }
    public int getPrice() {
        return price;
    }
    public int getBedrooms() {
        return bedrooms;
    }
    public int getBathrooms() {
        return bathrooms;
    }
    public int getSize() {
        return size;
    }
    public String getDescription() {
        return description;
    }
    public String getZip() {
        return zip;
    }
    public String getCity() {
        return city;
    }
    public int getLatitude() {
        return latitude;
    }
    public int getLongitude() {
        return longitude;
    }

    @Override
    public String toString() {
        return "HouseResponse{" +
                "id='" + id + '\'' +
                ", image='" + image + '\'' +
                ", price=" + price +
                ", bedrooms=" + bedrooms +
                ", bathrooms=" + bathrooms +
                ", size=" + size +
                ", description='" + description + '\'' +
                ", zip='" + zip + '\'' +
                ", city='" + city + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", createdDate='" + createdDate + '\'' +
                '}';
    }

    /**used to sort the list by the lowest price*/
    @Override
    public int compareTo(House o) {
        return (this.getPrice() < o.getPrice() ? -1 :
                (this.getPrice() == o.getPrice() ? 0 : 1));
    }
}
