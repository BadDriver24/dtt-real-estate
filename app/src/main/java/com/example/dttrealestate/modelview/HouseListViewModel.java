package com.example.dttrealestate.modelview;



import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import com.example.dttrealestate.ApiResponse;
import com.example.dttrealestate.repository.HouseRepository;
import com.example.dttrealestate.model.House;
import java.util.ArrayList;
import java.util.List;

public class HouseListViewModel extends ViewModel {
    private MediatorLiveData<ApiResponse> mediatorLiveData;
    private HouseRepository houseRepository;
    private String searchBarText;

    //used just for the fetched houses, so it won't have to take them from the api and recalculate the distance when the phone is rotated
    private List<House> fetchedHouses = new ArrayList<>();
    //used to save the list of found houses, so when the phone is rotated, it won't be lost
    private List<House>modifiedListHouse = new ArrayList<>();
    /**the recyclerview is updated every time the location is changed (so the distance to a house is shown in realtime) only when this is true
    updateHouses will be false while the user is searching for a specific house so the list will stay with his searches
    will get back true after the user is done with his searching*/
    private boolean updateHouses = true;

    public HouseListViewModel() {
        this.mediatorLiveData = new MediatorLiveData<>();
        houseRepository = HouseRepository.getHouseRepository();
    }

    /**get the response code from the API so it can be handled in the activity*/
    public LiveData<ApiResponse> getResponse(){
        mediatorLiveData.addSource(houseRepository.fetchApi(),
                new Observer<ApiResponse>() {
                    @Override
                    public void onChanged(ApiResponse apiResponse) {
                        mediatorLiveData.setValue(apiResponse);
                    }
                });
        return mediatorLiveData;
    }

    /**send the list of objects fetched from the API to the view (activity or fragment)*/
    public LiveData<List<House>> getHouses() {
        return houseRepository.getHouses();
    }

    /**call the fetch method from the repository*/
    public LiveData<ApiResponse> fetchApi(){
        return houseRepository.fetchApi();
    }


    /**go through the list of fetched houses and search by zip code or city*/
    public List<House> searchHouses(String query, List<House> houseList){
        Log.d("SEARCH", query);
        modifiedListHouse.clear();
        if(query != ""){
            setSearchBarText(query);
            for (House h : houseList){
                if(h.getZip().toLowerCase().contains(query.toLowerCase()) || h.getCity().toLowerCase().contains(query.toLowerCase())){
                    modifiedListHouse.add(h);
                }
            }
            return modifiedListHouse;
        }
        else{
            return fetchedHouses;
        }
    }

    public List<House> getFetchedHouses() {
        return fetchedHouses;
    }

    public void setFetchedHouses(List<House> fetchedHouses) {
        this.fetchedHouses = fetchedHouses;
    }

    public List<House> getModifiedListHouse() {
        return modifiedListHouse;
    }

    public boolean isUpdateHouses() {
        return updateHouses;
    }

    public void setUpdateHouses(boolean updateHouses) {
        this.updateHouses = updateHouses;
    }

    public void setSearchBarText(String query){
        searchBarText = query;
    }

    public String getSearchBarText(){
        return this.searchBarText;
    }
}
