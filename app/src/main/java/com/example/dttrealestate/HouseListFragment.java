package com.example.dttrealestate;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.dttrealestate.adapter.HouseRecyclerAdapter;
import com.example.dttrealestate.adapter.OnHouseListener;
import com.example.dttrealestate.model.House;
import com.example.dttrealestate.modelview.HouseListViewModel;

import java.util.Collections;
import java.util.List;

public class  HouseListFragment extends Fragment implements OnHouseListener, LocationListener {

    private EditText searchBar;
    private Button searchButtonBar, closeButtonBar;
    //these 2 views are INVISIBLE by default, they will show only if no searches are available
    private ImageView imageViewNoFound;
    private TextView errorTextView;
    //recyclerview
    private HouseListViewModel houseListViewModel;
    private RecyclerView recyclerView;
    private HouseRecyclerAdapter recyclerAdapter;
    //used to get the current location
    LocationManager locationManager;
    FragmentManager fragmentManager = getFragmentManager();
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_house_list, container, false);
        //used to observe any changes in the House ModelView (which is fetched from API)
        houseListViewModel = ViewModelProviders.of(getActivity()).get(HouseListViewModel.class);
        houseListViewModel.fetchApi();
        getViewObjects(view);
        getResponseAPI();
        subscribeObservers();
        initRecycleView();
        searchBarEvents();
        locationPermission();
        navBar();
        searchBarFill();
        return view;
    }

    public void searchBarFill(){
        if(houseListViewModel.getSearchBarText() != null){
            if(!houseListViewModel.getSearchBarText().equals("")){
                searchBar.setText(houseListViewModel.getSearchBarText());
            }
        }
    }

    /**navbar is visible in this  fragment*/
    public void navBar(){
        View navBar = getActivity().findViewById(R.id.bottomNavBar);
        navBar.setVisibility(View.VISIBLE);
    }

    public void locationPermission(){
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[]{ Manifest.permission.ACCESS_FINE_LOCATION},100);
        }
    }

    public void getResponseAPI(){
        houseListViewModel.getResponse().observe(this, new Observer<ApiResponse>() {
            @Override
            public void onChanged(ApiResponse apiResponse) {
                if(apiResponse.getError() != 200){
                    errorTextView.setVisibility(View.VISIBLE);
                    errorTextView.setText("Error fetching data. Please try again later!");
                    searchBar.setEnabled(false);
                    searchButtonBar.setEnabled(false);
                    houseListViewModel.fetchApi();
                }
                else{
                    errorTextView.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    /**Events like, when the x button is pressed, the keyboard is hidden, or when the text from the
     * edit text is changed, the searchHouses method is called again*/
    public void searchBarEvents(){
        closeButtonBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchBar.clearFocus();
                searchBar.getText().clear();
                closeButtonBar.setVisibility(View.GONE);
                searchButtonBar.setVisibility(View.VISIBLE);
                stopSearch();
                hideKeyboard();
            }
        });
        searchBar.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    searchBar.clearFocus();
                    hideKeyboard();
                    return true;
                }
                return false;
            }
        });
        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                searchBarButtonsControl();
                searchHouses(searchBar.getText().toString());
            }
            @Override
            public void afterTextChanged(Editable s) { }
        });
    }
    /**If the search bar is empty, search icon will appear on the right side, otherwise, x will appear */
    public void searchBarButtonsControl(){
        if(searchBar.getText().toString().trim().length() == 0){
            closeButtonBar.setVisibility(View.GONE);
            searchButtonBar.setVisibility(View.VISIBLE);
        }
        else{
            closeButtonBar.setVisibility(View.VISIBLE);
            searchButtonBar.setVisibility(View.GONE);
            searchHouses(searchBar.getText().toString());
        }
    }

    /**if no results are found, an imageView and a textView telling the user that nothing was found will appear after every search
     * if the search has >0 results, these will be hidden */
    public void controlViewsSearch(){
        if(!houseListViewModel.isUpdateHouses() && houseListViewModel.getModifiedListHouse().isEmpty()){
            imageViewNoFound.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.INVISIBLE);
            errorTextView.setVisibility(View.VISIBLE);
        }
        else{
            imageViewNoFound.setVisibility(View.INVISIBLE);
            recyclerView.setVisibility(View.VISIBLE);
            errorTextView.setVisibility(View.INVISIBLE);
        }
    }

    public void getViewObjects(View view){
        errorTextView = view.findViewById(R.id.textViewError);
        recyclerView = view.findViewById(R.id.recycleView);
        imageViewNoFound = view.findViewById(R.id.imageViewNoSearch);
        searchBar = view.findViewById(R.id.editTextSearchBar);
        searchButtonBar = view.findViewById(R.id.buttonSearchBarSearch);
        closeButtonBar = view.findViewById(R.id.buttonSearchbarClose);
    }

    private void initRecycleView(){
        recyclerAdapter = new HouseRecyclerAdapter(this);
        recyclerView.setAdapter(recyclerAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    /**if there are any changes in the House ViewModel, the recyclerView will update the data.
    list of fetched houses is also updated
    this will verify if the fetched data from the api is different than the already fetched list from the viewModel. If there is a difference, it will also update the location
    which is not fetched from the api and it's manually calculated;
     the location is also updated if there is no difference, but the position of the phone is moved*/
    private void subscribeObservers(){
        houseListViewModel.getHouses().observe(this, new Observer<List<House>>() {
            @Override
            public void onChanged(List<House> houseResponse) {
                if(!houseListViewModel.isUpdateHouses()) {
                    recyclerAdapter.setHouses(houseListViewModel.getModifiedListHouse());
                }
                else{
                    if(houseResponse != null) {
                        if(houseListViewModel.getFetchedHouses() == null || houseListViewModel.getFetchedHouses().size() < houseResponse.size()){
                            new sortTask().execute(houseResponse);
                            recyclerAdapter.setHouses(houseResponse);
                            houseListViewModel.setFetchedHouses(houseResponse);
                            getLocation();
                        }
                        else{
                            recyclerAdapter.setHouses(houseListViewModel.getFetchedHouses());
                        }
                    }
                }
            }
        });
    }

    /** go through the list of fetched houses and search by zip code or city
    it will update  the modifiedList of houses from the viewModel, so when the phone is rotated, the list of searches is not lost */
    public void searchHouses(String query){
        houseListViewModel.setUpdateHouses(false);
        List<House> modifiedListHouse;
        modifiedListHouse = houseListViewModel.searchHouses(query, houseListViewModel.getFetchedHouses());
        recyclerAdapter.setHouses(modifiedListHouse);
        errorTextView.setText("No results found!\nPerphaps try another search? ");
        controlViewsSearch();
    }

    /**called when the x button from the search bar is pressed, so the initial list of houses is shown */
    public void stopSearch(){
        houseListViewModel.setUpdateHouses(true);
        recyclerAdapter.setHouses(houseListViewModel.getFetchedHouses());
        controlViewsSearch();
    }

    /**called when an item from the list is clicked. */
    @Override
    public void onHouseClick(int position) {
        Log.d("HouseListFragment", recyclerAdapter.getHouse(position).getBathrooms() + "");
        fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        HouseInfoFragment houseInfoFragment = new HouseInfoFragment();
        Bundle args = new Bundle();
        args.putSerializable("houseObject", recyclerAdapter.getHouse(position));
        houseInfoFragment.setArguments(args);
        fragmentTransaction.replace(R.id.frameLayout, houseInfoFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    /**when the current location is changed, this methods will be called
    this will get your current coordinates and calculate the distance in km to the coordinates of every fetched house in the list */
    @Override
    public void onLocationChanged(Location location) {
        float[] result = new float[1];
        for(House h : houseListViewModel.getFetchedHouses()){
            Location.distanceBetween(location.getLatitude(), location.getLongitude(), h.getLatitude(), h.getLongitude(), result);
            h.setDistanceTo((int)(result[0] / 1000));
        }
        if(houseListViewModel.isUpdateHouses()){
            recyclerAdapter.setHouses(houseListViewModel.getFetchedHouses());
        }
    }

    //methods which are not used from LocationListener interface
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) { }
    @Override
    public void onProviderEnabled(String provider) { }
    @Override
    public void onProviderDisabled(String provider) { }

    /** SuppressLint is here to stop showing errors in ide about location permission */
    @SuppressLint("MissingPermission")
    private void getLocation() {
        try {
            locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,0,0,this);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,0,this);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /** hide the keyboard when the x button is pressed from the search bar*/
    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(searchBar.getWindowToken(), 0);
    }

    /** Another thread created to search through the list of houses */
    private class sortTask extends AsyncTask<List<House>, Integer, String>{
        @Override
        protected void onPreExecute() {
            errorTextView.setText("Fetching data...");
            errorTextView.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(List<House>... lists) {
            Collections.sort(lists[0]);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            errorTextView.setVisibility(View.GONE);
        }
    }

    /**get the list of houses again after the location permission was given, so it can calculate the distance
    also show an alert dialog every time the app is opened to remind the user to give location permission */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode==100){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLocation();
                subscribeObservers();
            }
            else{
                showAlert("Please give location permission to see distance to houses");
            }
        }
    }

    public void showAlert(String message){
        if(((MainActivity)getActivity()).showDialog){
            AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
            alertDialog.setTitle(message);
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Permission Settings",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent i = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + BuildConfig.APPLICATION_ID));
                            startActivity(i);
                            dialog.dismiss();
                        }
                    });
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No, Thanks!", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
            ((MainActivity)getActivity()).showDialog = false;
        }
    }
}
