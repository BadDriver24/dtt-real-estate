package com.example.dttrealestate;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.request.RequestOptions;
import com.example.dttrealestate.model.House;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.NumberFormat;

public class HouseInfoFragment extends Fragment implements OnMapReadyCallback {
    private MapView mapView;
    private GoogleMap mMap;
    private House house;
    private TextView price, bedrooms, bathrooms, layers, distanceTo, description;
    private ImageView imageView, imageViewFixScroll, imageViewDistanceToIcon;
    private Button buttonBack;
    private ScrollView scrollView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_house_info, container, false);
        /**receive the house object from the houseList fragment*/
        house = (House) getArguments().getSerializable("houseObject");
        fillViews(view, house, savedInstanceState);
        hideNavBar();
        backButton();
        hideDistanceIcon();
        fixScrollView();
        return view;
    }

    /**in this fragment, the bottom navBar will be invisible*/
    public void hideNavBar(){
        View navBar = getActivity().findViewById(R.id.bottomNavBar);
        navBar.setVisibility(View.GONE);
    }

    /**hide distance icon if location is not available*/
    public void hideDistanceIcon(){
        if(house.getDistanceTo() == 0){
            distanceTo.setVisibility(View.GONE);
            imageViewDistanceToIcon.setVisibility(View.GONE);
        }
        else{
            distanceTo.setVisibility(View.VISIBLE);
            imageViewDistanceToIcon.setVisibility(View.VISIBLE);
        }
    }

    /**back button located on the top of the imageView with the house*/
    public void backButton(){
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                fm.popBackStack();
            }
        });
    }

    /**fixed the vertical scroll view on google maps by applying a transparent imageView on top of the map,
    and while the image is touched, scrollview is disabled */
    public void fixScrollView(){
        imageViewFixScroll.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        scrollView.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        scrollView.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        scrollView.requestDisallowInterceptTouchEvent(true);
                        return false;

                    default:
                        return true;
                }
            }
        });
    }

    /**fill the views with data from the object */
    public void fillViews(View view, House house, Bundle savedInstanceState){
        buttonBack = view.findViewById(R.id.buttonHouseInfoBack);
        imageViewFixScroll = view.findViewById(R.id.imageViewFixScroll);
        scrollView = view.findViewById(R.id.scrollView);
        // Gets the MapView from the XML fragment_house_info and creates it
        mapView = view.findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        price = view.findViewById(R.id.textViewHouseInfoPrice);
        bedrooms = view.findViewById(R.id.textViewHouseInfoBedrooms);
        bathrooms = view.findViewById(R.id.textViewHouseInfoBathrooms);
        layers = view.findViewById(R.id.textViewHouseInfoLayers);
        distanceTo = view.findViewById(R.id.textViewHouseInfoDistanceTo);
        description = view.findViewById(R.id.textView1HouseInfoDescription);
        imageView = view.findViewById(R.id.imageViewHouseInfoImage);
        imageViewDistanceToIcon = view.findViewById(R.id.imageViewDistanceToIcon);
        price.setText("\u20ac" + NumberFormat.getInstance().format(house.getPrice()));
        bedrooms.setText(house.getBedrooms() + "");
        bathrooms.setText(house.getBathrooms() + "");
        layers.setText(house.getSize() + "");
        distanceTo.setText(house.getDistanceTo() + " km");
        description.setText(house.getDescription());
        RequestOptions requestOptions = new RequestOptions()
                .placeholder(R.drawable.ic_launcher_background);
        GlideUrl glideUrl = new GlideUrl(
                "https://intern.docker-dev.d-tt.nl" + house.getImage(),
                new LazyHeaders.Builder().addHeader("access-key", "98bww4ezuzfePCYFxJEWyszbUXc7dxRx").build());
        Glide.with(getContext())
                .setDefaultRequestOptions(requestOptions)
                .load(glideUrl)
                .into(imageView);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng latLng = new LatLng(house.getLatitude(), house.getLongitude());
        mMap.addMarker(new MarkerOptions().position(latLng).title(house.getCity() + " " + house.getZip()));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12f));
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

}
