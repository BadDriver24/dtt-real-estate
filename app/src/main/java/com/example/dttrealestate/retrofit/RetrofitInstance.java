package com.example.dttrealestate.retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**class used to make network requests using retrofit to take json from api directly as java objects*/
public class RetrofitInstance {
    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://intern.docker-dev.d-tt.nl")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    private static IHouseClient houseClient = retrofit.create(IHouseClient.class);

    public static IHouseClient getHouseApi() {
        return houseClient;
    }
}
