package com.example.dttrealestate.retrofit;

import com.example.dttrealestate.model.House;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

/**GET HOUSES REQUEST
 in the header, api key is passed*/
public interface IHouseClient {
    @Headers("access-key: 98bww4ezuzfePCYFxJEWyszbUXc7dxRx")
    @GET("api/house")
    Call<List<House>> getHouses();
}
