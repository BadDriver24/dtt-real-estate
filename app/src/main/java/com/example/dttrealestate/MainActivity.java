package com.example.dttrealestate;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity{
    public boolean showDialog = false;
    /** show the houseListFragment at the starting of the application
     set the dialog on true, so the dialog about location appears only once when the app is opened, not every time you go back to house list */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(null == savedInstanceState) {
            showDialog = true;
            showInitialHouseListFragment();
        }
        navBarFragments();
    }
    public void showInitialHouseListFragment(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        HouseListFragment houseListFragment = new HouseListFragment();
        fragmentTransaction.add(R.id.frameLayout, houseListFragment, "houseListFragment");
        fragmentTransaction.commit();
    }

    public void navBarFragments(){
        final BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavBar);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                //if the users clicks on the home button from navBar, houseList fragment will appear, if it clicks on info_nav button, about fragment will appear
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                switch (item.getItemId()) {
                    case R.id.home_nav:
                        HouseListFragment houseListFragment = new HouseListFragment();
                        fragmentTransaction.replace(R.id.frameLayout, houseListFragment);
                        fragmentTransaction.commit();
                        break;
                    case R.id.info_nav:
                        AboutFragment aboutFragment = new AboutFragment();
                        fragmentTransaction.replace(R.id.frameLayout, aboutFragment);
                        fragmentTransaction.commit();
                        break;
                }
                return true;
            }
        });
    }


}