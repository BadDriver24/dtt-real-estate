package com.example.dttrealestate.remotedatasource;


import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.example.dttrealestate.ApiResponse;
import com.example.dttrealestate.model.House;
import com.example.dttrealestate.retrofit.IHouseClient;
import com.example.dttrealestate.retrofit.RetrofitInstance;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HouseRemoteSource {
    private static HouseRemoteSource houseRemoteSource;
    private MutableLiveData<List<House>> mHouses = new MutableLiveData<>();


    /**create the object using skeleton pattern*/
    public static HouseRemoteSource getHouseRemoteSource() {
        if(houseRemoteSource == null){
            houseRemoteSource = new HouseRemoteSource();
        }
        return houseRemoteSource;
    }

    /**send the list of objects fetched from the API to the repository*/
    public MutableLiveData<List<House>> getHouses() {
        return mHouses;
    }

    /**fetch json from the api into a list of type House*/
    public LiveData<ApiResponse> fetchApi(){
        final MutableLiveData<ApiResponse> apiResponse = new MutableLiveData<>();
        IHouseClient houseClient = RetrofitInstance.getHouseApi();
        Call<List<House>> responseCall = houseClient.getHouses();
        responseCall.enqueue(new Callback<List<House>>() {
            @Override
            public void onResponse(Call<List<House>> call, Response<List<House>> response) {
                Log.d("HouseAPI", "server response code " + response.toString());
                apiResponse.postValue(new ApiResponse(response.code()));
                if(response.code() == 200){
                    List<House> houses = response.body();
                    mHouses.postValue(houses);
                }
                else {
                    Log.d("HouseAPI", "onResponse FAIL: " + response.code());
                }
            }
            @Override
            public void onFailure(Call<List<House>> call, Throwable t) {
                Log.d("HouseAPI", call.toString());
                apiResponse.postValue(new ApiResponse(401));
                t.printStackTrace();
            }
        });
        return apiResponse;
    }
}
