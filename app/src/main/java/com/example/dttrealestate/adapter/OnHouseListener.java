package com.example.dttrealestate.adapter;

public interface OnHouseListener {
    void onHouseClick(int position);
}
