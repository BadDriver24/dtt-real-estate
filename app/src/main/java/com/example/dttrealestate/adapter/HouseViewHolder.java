package com.example.dttrealestate.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.dttrealestate.R;

public class HouseViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    TextView price, address, bathrooms, bedrooms, layers, location;
    ImageView image, locationImage;
    OnHouseListener onHouseListener;

    public HouseViewHolder(@NonNull View itemView, OnHouseListener onHouseListener) {
        super(itemView);
        this.onHouseListener = onHouseListener;
        price = itemView.findViewById(R.id.textViewHousePrice);
        address = itemView.findViewById(R.id.textViewHouseAddress);
        image = itemView.findViewById(R.id.imageViewHouseImage);
        bathrooms = itemView.findViewById(R.id.textViewHouseInfoBedrooms);
        bedrooms = itemView.findViewById(R.id.textViewHouseInfoBathrooms);
        layers = itemView.findViewById(R.id.textViewHouseInfoLayers);
        location = itemView.findViewById(R.id.textViewHouseInfoDistanceTo);
        locationImage = itemView.findViewById(R.id.imageViewDistanceToIcon);
        itemView.setOnClickListener(this);
    }

    public void hideLocation(int distance){
        if(distance == 0){
            location.setVisibility(View.GONE);
            locationImage.setVisibility(View.GONE);
        }
        else{
            location.setVisibility(View.VISIBLE);
            locationImage.setVisibility(View.VISIBLE);
        }
    }

    /**this will send the position of the clicked element from the recyclerview to the HouseListFragment*/
    @Override
    public void onClick(View v) {
        onHouseListener.onHouseClick(getAdapterPosition());
    }
}
