package com.example.dttrealestate.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.request.RequestOptions;
import com.example.dttrealestate.R;
import com.example.dttrealestate.model.House;

import java.text.NumberFormat;
import java.util.List;

public class HouseRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<House> houses;
    private OnHouseListener onHouseListener;

    public HouseRecyclerAdapter(OnHouseListener onHouseListener) {
        this.onHouseListener = onHouseListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_house_list, parent, false);
        return new HouseViewHolder(view, onHouseListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        RequestOptions requestOptions = new RequestOptions()
                .placeholder(R.drawable.ic_launcher_background);
        GlideUrl glideUrl = new GlideUrl(
                "https://intern.docker-dev.d-tt.nl" + houses.get(position).getImage(),
                new LazyHeaders.Builder().addHeader("access-key", "98bww4ezuzfePCYFxJEWyszbUXc7dxRx").build());
        Glide.with(holder.itemView.getContext())
                .setDefaultRequestOptions(requestOptions)
                .load(glideUrl)
                .into(((HouseViewHolder)holder).image);
        ((HouseViewHolder)holder).address.setText(houses.get(position).getZip() + " " + houses.get(position).getCity());
        ((HouseViewHolder)holder).price.setText("\u20ac"+ NumberFormat.getInstance().format(houses.get(position).getPrice()));
        ((HouseViewHolder)holder).bathrooms.setText(houses.get(position).getBathrooms() + "");
        ((HouseViewHolder)holder).bedrooms.setText(houses.get(position).getBedrooms() + "");
        ((HouseViewHolder)holder).layers.setText(houses.get(position).getSize() + "");
        ((HouseViewHolder)holder).location.setText(houses.get(position).getDistanceTo() + " km");

        //hide the location marker from the list if distance is 0
        ((HouseViewHolder) holder).hideLocation(houses.get(position).getDistanceTo());
    }

    @Override
    public int getItemCount() {
        if(houses != null){
            return houses.size();
        }
        return 0;
    }

    public void setHouses(List<House> houseList){
        houses = houseList;
        notifyDataSetChanged();
    }

    public House getHouse(int position){
        return houses.get(position);
    }
}
