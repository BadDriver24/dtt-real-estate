package com.example.dttrealestate.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.dttrealestate.ApiResponse;
import com.example.dttrealestate.model.House;
import com.example.dttrealestate.remotedatasource.HouseRemoteSource;
import java.util.List;

public class HouseRepository {
    private static HouseRepository houseRepository;
    private HouseRemoteSource houseRemoteSource = HouseRemoteSource.getHouseRemoteSource();

    /**creates a HouseRepository object using skeleton pattern*/
    public static HouseRepository getHouseRepository() {
        if(houseRepository == null){
            houseRepository = new HouseRepository();
        }
        return houseRepository;
    }

    /**send the list of objects fetched from the API to the ViewModel*/
    public MutableLiveData<List<House>> getHouses() {
        return houseRemoteSource.getHouses();
    }

    /**call the fetch method from the remote source*/
    public LiveData<ApiResponse> fetchApi(){
        return houseRemoteSource.fetchApi();
    }

}
